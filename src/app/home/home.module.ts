import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';
import { HeaderComponent } from '../components/header/header.component';
import { SectionsComponent } from '../components/sections/sections.component';
import { VideoSectionComponent } from '../components/video-section/video-section.component';
import { SectionsOneComponent } from '../components/sections-one/sections-one.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule
  ],
  declarations: [HomePage, HeaderComponent, SectionsComponent, VideoSectionComponent, SectionsOneComponent]
})
export class HomePageModule {}
